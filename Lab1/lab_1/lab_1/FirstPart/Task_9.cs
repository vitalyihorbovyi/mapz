﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_1.FirstPart
{
    class OutRefCheck
    {
        public int Val { get; set; }
    }
    class OutRef
    {
        public void InitializeWithoutParameters(int num1, int num2)
        {
            num1 = 4; num2 = 4;
        }
        public void InitializeWithParameters(out int num1, ref int num2)
        {
            num1 = 5; num2 = 5;
        }
        public void WithoutRef(OutRefCheck obj)
        {
            obj.Val = 5;
        }
        public void WithRef(OutRefCheck obj)
        {
            obj.Val = 6;
        }
    }
}
