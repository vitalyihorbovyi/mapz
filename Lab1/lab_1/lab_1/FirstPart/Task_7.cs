﻿using System;
using System.Collections.Generic;

namespace lab_1.FirstPart
{
    class Parent
    {
        public Parent(int number)
        {
            Console.WriteLine($"Parent constructor got number - {number}");
        }
        public virtual void Print()
        {
            Console.WriteLine("Parent");
        }
    }
    class Child : Parent
    {
        int number1;
        int number2;
        int[] numbers;
        public Child(int number1, int number2) : base(number1)
        {
            this.number1 = number1;
            this.number2 = number2;
            numbers = new int[] { number1, number2 };
            Console.WriteLine($"Child constructor got numbers - {this.number1}, {this.number2}");
        }
        public Child() : this(2, 3) { }
        public int this[int index]
        {
            get 
            {
                if (index < 0 || index >= numbers.Length)
                {
                    throw new IndexOutOfRangeException("index");
                }
                return numbers[index]; 
            }
        }
        public override void Print()
        {
            Console.WriteLine("Child");
        }
    }
}
