﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_1.FirstPart
{
    class Myclass
    {
        public string GenerateFirstString() => "First text";
        internal class MyNestedClass
        {
            public string GenerateSecondString() => "Second text";
        }
    }
}
