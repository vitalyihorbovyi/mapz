﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_1.FirstPart
{
    class StaticDynamic
    {
        int[] statArr = new int[2];
        int[] dynamArr;
        static int staticVal;
        public StaticDynamic()
        {
            dynamArr = new int[2];
            Console.WriteLine("Dynamic");
        }
        static StaticDynamic()
        {
            staticVal = 0;
            Console.WriteLine("Static");
        }
    }
}
