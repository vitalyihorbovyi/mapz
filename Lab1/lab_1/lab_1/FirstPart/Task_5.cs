﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_1.FirstPart
{
    [Flags]
    public enum Colors : byte
    {
        Blue = 1,
        Yellow = 2,
        Green = 4,
        Red = 8,
        Purple = 16
    }
}
