﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_1.FirstPart
{
    class MyObj : Object
    {
        public override bool Equals(object obj)
        {
            return obj is MyObj;
        }
        public override int GetHashCode()
        {
            return 0;
        }
        public override string ToString()
        {
            return "MyObj";
        }
    }
}
