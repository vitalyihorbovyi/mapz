﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_1.FirstPart_2
{
    public interface IStudent
    {
        int Mark1 { get; set; }
        int Mark2 { get; set; }
        //private double GetAverageScore();
    }

    public abstract class Student
    {
        public abstract string GetName();
    }

    public class Vitaliy : Student, IStudent
    {
        public int Mark1 { get; set; }
        public int Mark2 { get; set; }
        public Vitaliy(int mark1, int mark2)
        {
            Mark1 = mark1;
            Mark2 = mark2;
        }
        public override string GetName() => "Vitaliy";
        public double GetAverageScore() => (Mark1 + Mark2) / 2;
    }
}
