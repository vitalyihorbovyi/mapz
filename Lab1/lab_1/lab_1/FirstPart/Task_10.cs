﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_1.FirstPart
{
    class ImplicitExplicit
    {
        public double Number { get; set; }
        public ImplicitExplicit(double number)
        {
            Number = number;
        }
        public static explicit operator ImplicitExplicit(double number) => new ImplicitExplicit(number);
        public static implicit operator double(ImplicitExplicit obj) => obj.Number;
    }
}
