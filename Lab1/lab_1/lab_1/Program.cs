﻿using System;
using System.Diagnostics;
using lab_1.FirstPart;
using lab_1.SecondPart;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace lab_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Task_3
            //CheckStruct checkStruct = new CheckStruct();
            //checkStruct.check = 0;

            //Task_4
            //Myclass myObject = new Myclass();
            //Console.WriteLine(myObject.GenerateFirstString());
            //Myclass.MyNestedClass myNestedObject = new Myclass.MyNestedClass();
            //Console.WriteLine(myNestedObject.GenerateSecondString());

            //Task_5
            //Colors green = Colors.Blue | Colors.Yellow;
            //Console.WriteLine(green);
            //Console.WriteLine(green.HasFlag(Colors.Blue | Colors.Yellow));
            //Console.WriteLine(green.HasFlag(Colors.Red));

            //Colors res = Colors.Purple & Colors.Yellow;
            //Console.WriteLine(res);

            //Colors blue = green ^ Colors.Yellow;
            //Console.WriteLine(blue);

            //Task_7
            //Child child = new Child(0, 1);
            //Console.WriteLine();
            //Child child2 = new Child();

            //Task
            //int number = 10;
            //object obj = number;
            //number = (int)obj;

            //Task_8
            //StaticDynamic staticDynamic = new StaticDynamic();

            //Task_9
            //OutRef outRef = new OutRef();
            //int num1, num2 = 0;
            //outRef.InitializeWithParameters(out num1, ref num2);
            //Console.WriteLine($"{num1}, {num2}");
            //outRef.InitializeWithoutParameters(num1, num2);
            //Console.WriteLine($"{num1}, {num2}");
            //OutRefCheck obj = new OutRefCheck { Val = 0};
            //outRef.WithoutRef(obj);
            //Console.WriteLine(obj.Val);
            //outRef.WithRef(obj);
            //Console.WriteLine(obj.Val);

            //Task_10
            //double num1 = 0.5;
            //ImplicitExplicit num2 = (ImplicitExplicit)num1;
            //Console.WriteLine(num2.Number);
            //num2.Number = 1.5;
            //num1 = num2;
            //Console.WriteLine(num1);

            //SecondPart
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            for (int i = 0; i < 10000000; ++i)
            {
                using (ClassTest classTest = new ClassTest()) { }
            }
            stopwatch.Stop();
            Console.WriteLine(stopwatch.Elapsed.ToString(@"m\:ss\.ffff"));

            stopwatch = new Stopwatch();
            stopwatch.Start();
            for (int i = 0; i < 10000000; ++i)
            {
                using (StructTest structTest = new StructTest()) { }
            }
            stopwatch.Stop();
            Console.WriteLine(stopwatch.Elapsed.ToString(@"m\:ss\.ffff"));

            stopwatch = new Stopwatch();
            stopwatch.Start();
            for (int i = 0; i < 10000000; ++i)
            {
                using (SealedClassTest sealedClassTest = new SealedClassTest()) { }
            }
            stopwatch.Stop();
            Console.WriteLine(stopwatch.Elapsed.ToString(@"m\:ss\.ffff"));
        }
    }
}
