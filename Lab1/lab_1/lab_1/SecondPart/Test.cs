﻿using System;
using System.Diagnostics;

namespace lab_1.SecondPart
{

    struct StructTest : IDisposable
    {
        int val1;
        int val2;
        int val3;
        int val4;
        int val5;
        public void Dispose() { }
    }
    class ClassTest : IDisposable
    {
        int val1;
        int val2;
        int val3;
        int val4;
        int val5;
        public void Dispose() { }
    }
    sealed class SealedClassTest : IDisposable
    {
        int val1;
        int val2;
        int val3;
        int val4;
        int val5;
        public void Dispose() { }
    }
}
