using UnityEngine;

public class EasyLevelFactory : MonoBehaviour, ILevelFactory
{
    public IMysteryShipView GetMysteryShipView()
    {
        return new EasyLevelMysteryShipView();
    }
    public IInvaderView GetInvaderView(float percentKilled, AnimationCurve speedCurve)
    {
        return new EasyLevelInvaderView(percentKilled, speedCurve);
    }
}