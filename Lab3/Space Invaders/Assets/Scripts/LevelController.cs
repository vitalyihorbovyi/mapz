using UnityEditor;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    public static GameObject Level;
    static LevelController()
    {
        Level = AssetDatabase.LoadAssetAtPath<GameObject>($"Assets/Factories/EasyLevelFactory.prefab");
    }
}
