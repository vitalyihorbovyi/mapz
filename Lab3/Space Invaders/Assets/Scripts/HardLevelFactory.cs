﻿using UnityEngine;

public class HardLevelFactory : MonoBehaviour, ILevelFactory
{
    public IMysteryShipView GetMysteryShipView()
    {
        return new HardLevelMysteryShipView();
    }
    public IInvaderView GetInvaderView(float percentKilled, AnimationCurve speedCurve)
    {
        return new HardLevelInvaderView(percentKilled, speedCurve);
    }
}