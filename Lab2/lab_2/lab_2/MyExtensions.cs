﻿using System;
using System.Collections.Generic;

namespace MyFuncs
{
    public static class MyExtensions
    {
        public static string GeneratePet(this Person person)
        {
            return new List<string>() { "Cat", "Dog", "Parrot" }[new Random().Next(0, 3)];
        }
        public static int GenerateAge(this Person person)
        {
            return 30;
        }
        public static Gender GenerateGender(this Person person)
        {
            return (Gender)new Random().Next(0, 2);
        }
    }

}
