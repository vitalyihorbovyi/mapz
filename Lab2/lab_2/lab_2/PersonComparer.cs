﻿using System.Collections.Generic;

namespace MyFuncs
{
    public class PersonComparer : IComparer<Person>
    {
        public int Compare(Person person1, Person person2)
        {
            if (person1.Age > person2.Age)
                return 1;
            else if (person1.Age < person2.Age)
                return -1;
            return 0;
        }
    }
}
