﻿using System;
using System.Collections.Generic;

namespace MyFuncs
{
    public class Person 
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public int PetAmount { get; set; }
        public override string ToString()
        {
            return $"Person:\n\tName: {Name},\n\tAge: {Age},\n\tpets: {PetAmount}";
        }         
    }
}
