﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyFuncs
{
    public class Pet
    {
        public string OwnerName { get; set; }
        public string Animal { get; set; }
    }
}
